import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatDialogComponent } from './chat-dialog/chat-dialog.component';
import {FormsModule} from '@angular/forms';
import {ChatService} from '../service/chat.service';



@NgModule({
  declarations: [ChatDialogComponent],
  exports: [ ChatDialogComponent ],
  imports: [
    CommonModule,
    FormsModule
  ],
  providers: [ChatService]
})
export class ChatModule { }
