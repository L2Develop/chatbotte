import { Component, OnInit } from '@angular/core';
import {ChatService, Message} from '../../service/chat.service';
import {Observable} from 'rxjs';
import { scan } from 'rxjs/operators';

@Component({
  selector: 'app-chat-dialog',
  templateUrl: './chat-dialog.component.html',
  styleUrls: ['./chat-dialog.component.css']
})
export class ChatDialogComponent implements OnInit {

  messages: Observable<Message[]>;
  formValue: string;

  constructor(public chat: ChatService) { }

  ngOnInit() {
    // appends to array after each new message is added to feedSource

    this.messages = this.chat.conversation.asObservable()
      .pipe(scan((acc, val) => acc.concat(val) ));
  }

  sendMessage() {
    if (this.formValue === '') {
      return;
    } else {
      this.chat.converse(this.formValue);
      this.formValue = '';
    }
  }

  clickNuit() {
   document.getElementById('nuitbtn').style.display = 'block';
  }
  clickde() {
    document.getElementById('debtn').style.display = 'block';
  }
  clickInfo() {
    document.getElementById('infobtn').style.display = 'block';
  }

}
